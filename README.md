# Chessboard Corner Selection Tools
> User interface for easy corner selection of highly distorted images

> Try online: https://calib.herokuapp.com/

You have to select chessboard area with brush and Harris detector will automatically find corners. Additional precision is obtained using subpixel corner detection. If detection fails you can use a rubber to delete points and brush to draw them again. The points can be exported as CSV file.

NOTICE: Super early version!

![Demo](https://i.imgur.com/3GwNPpQ.gif)

More: https://i.imgur.com/XehlL9l.jpg

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# run backend
python backend/app.py
```
