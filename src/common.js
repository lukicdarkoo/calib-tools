import axios from "axios";

const api = axios.create({
    baseURL: API_URL,
    timeout: 3000,
    withCredentials: true,
});

export default api;
