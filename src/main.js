import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import Upload from './pages/Upload.vue'
import Corners from './pages/Corners.vue'
import 'bootstrap';
import VueSlider from 'vue-slider-component'
import 'vue-slider-component/theme/default.css'
import Bootstrap from 'bootstrap/dist/css/bootstrap.css'; 
import './assets/style.css'
import ShortKey from 'vue-shortkey';




Vue.component('VueSlider', VueSlider)
Vue.use(VueRouter)
Vue.use(ShortKey)

const NotFound = { template: '<div>Not found</div>' }

const routes = [
  { path: '/', component: Upload },
  { path: '/corners', component: Corners },
  { path: '*', component: NotFound }
]

const router = new VueRouter({
  routes: routes,
})

new Vue({
  el: '#app',
  render: h => h(App),
  router: router,
})
