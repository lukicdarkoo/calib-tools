from flask import Flask, request, send_file, make_response, jsonify, session, send_from_directory
from flask_cors import CORS, cross_origin
import os
import cv2
import numpy as np
from numpy.linalg import norm
from flask_session import Session, FileSystemSessionInterface  


app = Flask(__name__, static_url_path='')

app.config['SESSION_TYPE'] = 'filesystem'
session_ext = Session()
session_ext.init_app(app)  
CORS(app, supports_credentials=True, resources={r"/*": {"origins": "*"}})



@app.route('/remove', methods=['GET'])
def remove():
    x = int(request.args.get('x'))
    y = int(request.args.get('y'))
    w = int(request.args.get('w'))
    h = int(request.args.get('h'))

    # Convert to absolute
    old_corners = session['corners']
    session['corners'] = []
    for corner in old_corners:
        if not ((x - w//2 < corner[0] < x + w//2) and (y + h//2 > corner[1] > y - h//2)):
            session['corners'].append(corner)
    
    session.modified = True
    
    # Return corners
    return jsonify(session['corners'])


@app.route('/list', methods=['GET'])
def listall():
    return jsonify(session['corners'])


@app.route('/clear', methods=['GET'])
def clear():
    session['corners'] = []
    return jsonify(session['corners'])


@app.route('/next', methods=['GET'])
def next():
    n = int(request.args.get('n', default=1))

    for _ in range(n):
        last = session['corners'][-1]
        blast = session['corners'][-2]
        filename = session['img']
        img = cv2.imread(filename)
        gray = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)

        next_c = [ 
            2 * last[0] - blast[0],
            2 * last[1] - blast[1],
        ]
        next_c_t = np.array([[ next_c ]]).astype(np.float32)
        
        corners = cv2.cornerSubPix(
            gray, 
            next_c_t, 
            (15, 15), 
            (-1, -1), 
            (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 30, 0.001)
        )

        session['corners'].append([
            float(corners[0, 0, 0]),
            float(corners[0, 0, 1]),
        ])

    return jsonify(session['corners'])

@app.route('/add', methods=['GET'])
def add():
    x = int(request.args.get('x'))
    y = int(request.args.get('y'))
    w = int(request.args.get('w'))

    # Load image
    filename = session['img']
    img = cv2.imread(filename)
    gray = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)

    # Convert to absolute
    corners = np.array([[ [x, y] ]]).astype(np.float32)
    if w > 5:
        corners = cv2.cornerSubPix(
            gray, 
            corners, 
            (w, w), 
            (-1, -1), 
            (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 30, 0.001)
        )

    # Put corners to session
    overlap = False
    for corner_prev in session['corners']:
        if abs(corner_prev[0] - x) < 2 and abs(corner_prev[1] - y) < 2:
            overlap = True

    if not overlap:
        session['corners'].append([
            float(corners[0, 0, 0]),
            float(corners[0, 0, 1]),
        ])
    
    # Return corners
    return jsonify(session['corners'])


@app.route('/upload', methods=['POST'])
def upload():
    img = request.files['file']
    full_filename = os.path.join('/tmp', img.filename)
    img.save(full_filename)
    session['img'] = full_filename
    session['corners'] = []
    return jsonify({'success': True})


@app.route('/csv', methods=['GET'])
def csv():
    csv_content = '\r\n'.join([ f'{i},{x},{y}' for i, (x, y) in enumerate(session['corners']) ])
    filename = os.path.splitext(os.path.basename(session['img']))[0]

    output = make_response(csv_content)
    output.headers['Content-Disposition'] = f'attachment; filename={filename}.csv'
    output.headers['Content-type'] = 'text/csv'
    return output


@app.route('/image', methods=['GET'])
def image():
    if 'img' not in session:
        folder = os.path.dirname(os.path.realpath(__file__))
        session['img'] = os.path.join(folder, '../', 'src', 'assets', 'sample.jpg')
        session['corners'] = []
    return send_file(session['img'])


@app.route('/dist/<path:path>')
def serve_dist(path):
    return send_from_directory('../dist', path)


@app.route('/')
def root():
    return open('index.html').read()


if __name__ == "__main__":
    port = int(os.environ.get('PORT', 5000))
    app.secret_key = 'NekaStrasnaSifra'
    app.debug = True
    app.run(host='0.0.0.0', port=port)
