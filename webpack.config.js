const path = require('path')
const webpack = require('webpack')
const { VueLoaderPlugin } = require('vue-loader')
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');


module.exports = (env, options) => {
  const API_URL = (options.mode === 'production') ? '' : 'http://127.0.0.1:5000'
  console.log(`Running Webpack ${webpack.version} in 'mode': ${options.mode}`);
  console.log(`API URL: ${API_URL}`);

  exported = {
    entry: './src/main.js',
    output: {
      path: path.resolve(__dirname, './dist'),
      publicPath: '/dist/',
      filename: 'build.js'
    },
    module: {
      rules: [
        {
          test: /\.css$/,
          use: [
            'vue-style-loader',
            'css-loader'
          ],
        }, {
          test: /\.vue$/,
          loader: 'vue-loader',
          options: {
            loaders: {
            }
            // other vue-loader options go here
          }
        },
        {
          test: /\.js$/,
          loader: 'babel-loader',
          exclude: /node_modules/
        },
        {
          test: /\.(png|jpg|gif|svg)$/,
          loader: 'file-loader',
          options: {
            name: '[name].[ext]?[hash]'
          }
        }
      ]
    },
    resolve: {
      alias: {
        'vue$': 'vue/dist/vue.esm.js'
      },
      extensions: ['*', '.js', '.vue', '.json']
    },
    devServer: {
      historyApiFallback: true,
      noInfo: true,
      overlay: true
    },
    performance: {
      hints: false
    },
    plugins: [
      new VueLoaderPlugin(),
      new webpack.DefinePlugin({
        API_URL: JSON.stringify(API_URL) ,
      }),
    ],
    devtool: '#eval-source-map'
  }

  if (options.mode === 'production') {
    exported.optimization = {
      minimizer: [
        new UglifyJsPlugin({
          cache: true,
          parallel: true,
          uglifyOptions: {
            compress: true,
            ecma: 6,
            mangle: true
          },
          sourceMap: false
        })
      ]
    }
  }

  return exported;
}
